import random
import math
import events
import itertools


# class Population:
#     def __init__(self, size, fitness=None, child_type=None):
#         self.size = size
#         self.fitness = fitness
#         self.child_type = child_type or Dot
#         self.pop = [child_type() for x in range(size)]

#     @property
#     def sorted(self):
#         key = self.fitness
#         yield from sorted(self.pop, key=key)

#     @property
#     def fittest(self):
#         return list(self.sorted)[0]

#     @property
#     def worst(self):
#         return list(self.sorted)[-1]

#     def update(self):
#         for child in self.pop:
#             child.update()
#         return None

#     def set_all(self, attribute, value):
#         for child in self.pop:
#             setattr(child, attribute, value)
#         return None

#     def breed(self, parents):
#         parents = list(parents)
#         for i in range(self.size):
#             daddy_pool = parents.copy()
#             choice = random.randint(0, len(parents) - 1)
#             mommy = parents[choice]
#             del daddy_pool[choice]
#             daddy = random.choice(daddy_pool)
#             yield self.combine(mommy, daddy)
#         raise StopIteration

#     def combine(self, mommy, daddy):
#         mom_brain = mommy.memory
#         dad_brain = daddy.memory
#         size = len(mom_brain)
#         right = math.floor(size/2)
#         left = math.ceil(size/2)

#         right_hem = mom_brain[:right]
#         left_hem = dad_brain[left:]
#         new_brain = (right_hem + left_hem)
#         child = self.child_type(iter(new_brain))
#         return child

#     def next_generation(self, children):
#         self.pop = list(children)
#         return None


class Dot:

    memory = None
    champion = False

    def __init__(self, new_brain=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.brain = new_brain or random_brain()
        self.memory = []

    def __repr__(self):
        name = self.__class__.__name__
        return '{}(position={}, champion={})'.format(
                name, str(self.position), self.champion)

    def reset_brain(self):
        # I'm pretty sure this method does not work.
        self.brain = iter(self.memory.copy())
        self.memory = []
        return None

    def update(self):
        thought = next(self.brain)
        self.memory.append(thought)
        return thought

    def mutate(self, chance):
        brain = itertools.chain(iter(self.brain), random_brain())
        self.memory = []
        self.brain = mutated_brain(brain, chance)
        return None

    def clone(self):
        brain = self.memory
        child = self.__class__(itertools.chain(iter(brain), random_brain()))
        return child


def mutated_brain(brain, mutation_rate):
    while True:
        roll = random.uniform(0, 1)
        thought = next(brain)
        if roll < mutation_rate:
            thought = random_thought()
        yield thought
    raise StopIteration


def random_brain():
    while True:
        yield random_thought()
    raise StopIteration


def random_thought():
    direction = math.radians(random.randint(0, 7)*45)
    x = math.cos(direction)
    y = math.sin(direction)
    return (x, y)


def mutate(children, chance=0.1):
    for child in children:
        if child.champion:
            continue
        child.mutate(chance)
        yield child
    raise StopIteration


# def apply_life(universe, age_limit=100, life_tick=10):
#     while True:
#         age = 0
#         while age < age_limit:
#             for life in range(life_tick):
#                 item = next(universe)
#                 yield item
#             yield events.LIFE
#             age += 1
#         yield events.DEATH
#     raise StopIteration


# def apply_evolution(universe, selection, mutation, population, chance=0.01,
#                     champion=True):
#     for event in universe:
#         if event == events.LIFE:
#             population.update()
#         if event == events.DEATH:
#             parents = selection(population.pop)
#             children = population.breed(parents)
#             mutated = mutation(children, chance)

#             if champion:
#                 champ = population.fittest.clone()
#                 champ.champion = True
#                 mutated = [champ] + list(mutated)[:-1]
#             population.next_generation(mutated)
#         yield event
#     raise StopIteration


class Population:
    def __init__(self, cap=100, child=None):
        self.cap = cap
        self.child = child or Dot()
        self.people = [self.child() for x in range(cap)]

    @property
    def living(self):
        return any(p.living for p in self.people)

    def update(self):
        for child in self.people:
            child.update()
        return None

    def evolve(self, selection, mutation, fittest=None, mutation_rate=0.1):
        parents = selection(self.people)
        children = self.breed(parents)
        mutated = list(mutation(children, mutation_rate))

        if fittest:
            champ = sorted(self.people, key=fittest)[0].clone()
            champ.champion = True
            mutated[0] = champ

        self.people = mutated
        return self.people

    def breed(self, parents):
        parents = list(parents)
        for i in range(self.cap):
            daddy_pool = parents.copy()
            choice = random.randint(0, len(parents) - 1)
            mommy = parents[choice]
            del daddy_pool[choice]
            daddy = random.choice(daddy_pool)
            yield self.combine(mommy, daddy)
        raise StopIteration

    def combine(self, mommy, daddy):
        mom_brain = mommy.memory
        dad_brain = daddy.memory
        size = len(mom_brain)
        right = math.floor(size/2)
        left = math.ceil(size/2)

        right_hem = mom_brain[:right]
        left_hem = dad_brain[left:]
        new_brain = (right_hem + left_hem)
        child = self.child(iter(new_brain))
        return child


def life(population, age_limit=100):
    while True:
        for i in range(age_limit):
            if not population.living:
                break
            yield events.LIFE
        yield events.DEATH
    raise StopIteration


def apply_life(universe, life, life_step=10):
    for index, tick in enumerate(universe):
        yield tick
        if index % life_step == 0:
            yield next(life)
    raise StopIteration
