#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import random
import time
import math
import itertools
import functools

import ui
import ga
import events
import physics


# The placement of the goal entity
GOAL = (480, 480)

# The size of each generation.
POPULATION_SIZE = 100

# The amount of thoughts / moves each child can have
MOVES = 100

# The speed at which physics.Universe() produces events.TICK
UNIVERSE_TICKRATE = 0.01


class Dot(ga.Dot, physics.AcceleratingEntity):

    """
    A seemingly hackish implementation of both ga.Dot and
    physics.AcceleratingEntity.
    """

    acceleration_multiplier = 10
    velocity_multiplier = 100
    size = (2,2)
    position = (10, 10)
    reached_goal = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        super(ga.Dot).__init__()

    def update(self):
        if self.living:
            x, y = super().update()
            self.set_acceleration(x, y)
            self.living = self.check_bounds()
        return None, None

    def check_bounds(self):
        if not self.living:
            return False
        x, y = self.position
        return 0 < x < 500 and 0 < y < 500

    def collide(self, item):
        if isinstance(item, Goal):
            self.reached_goal = True
        self.living = False
        return None

    def physical_move(self, t):
        if self.living:
            super().physical_move(t)
        return None


class Goal(physics.StaticEntity):

    """GOAAAAAAAL!!!"""

    position = GOAL
    size = (5, 5)


class Obstacle(physics.StaticEntity):

    """Obstacle is a StaticEntity that represents a wall."""

    def __init__(self, x, y, width, height):
        super().__init__()
        self.position = (x, y)
        self.size = (width, height)


def fitness(dot):
    """fitness(physics.AcceleratingEntity) -> int
    Calculate the fitness of a dot and return it in numerical form.
    """
    x, y = dot.position
    x2, y2 = GOAL
    distance = math.sqrt((x2 - x) ** 2 + (y2 - y) ** 2)
    return (-1.0 / (distance ** 2))


def selection(population):
    """selectiion([]physics.AcceleratingEntity) -> sorted(breeders, key=fitness)
    Selects a breeding stock and returns them sorted based on fitness.
    """
    selection_count = 5
    selection_group = 10

    # TODO: Make this less of an eye-sore
    breeders = [sorted([random.choice(population) for x in range(selection_group)], key=fitness)[0] for x in range(selection_count)]
    return sorted(breeders, key=fitness)


# (min, max)
def random_obstacles(x=(0, 100), y=(0, 100), width=(10, 100), height=(10, 50)):
    """random_obstacles(x, y, width, height) -> generator(Obstacle)
    This randomly generates obstacles. Each param is given in (min, max)
    pairs.
    """
    while True:
        xpos = random.randint(*x)
        ypos = random.randint(*y)
        w = random.randint(*width)
        h = random.randint(*height)
        obs = Obstacle(xpos, ypos, w, h)
        yield obs
    raise StopIteration


def main():
    window = ui.init()

    # Setup all the variables.
    moves = MOVES   # The number of thoughts life currently has.

    # Obstacles
    random_obs = random_obstacles(x=(100, 400), y=(100, 400))
    obstacles = list(itertools.islice(random_obs, 3))

    # Population and goal
    population = ga.Population(POPULATION_SIZE, Dot)
    goal = Goal()

    # Universe and collision
    universe = physics.Universe(population.people + [goal] + obstacles,
                                tickrate=UNIVERSE_TICKRATE)
    universe.add_collision(Dot, Goal)
    universe.add_collision(Dot, Obstacle)

    # And add a sprinkle of life.
    life = ga.apply_life(universe, ga.life(population, moves))

    start = time.time()
    generation = 0

    # Iterate forever
    while True:
        # Handle the UI events.
        ui.handle_events()

        # Yield an event from life
        event = next(life)

        # If the event was a universe tick, update the universe.
        # This also applies physics to all entities.
        if event == events.TICK:
            duration = start - time.time()
            universe.update(duration)
            start = time.time()

        # If the event was life then the population will get updated.
        # This event gives the Dots and new direction.
        # events.TICK and events.LIFE DO NOT have to come in at the same rate
        elif event == events.LIFE:
            population.update()

        # If the event was death this means all population is dead.
        # or the move limit has been reached. Whichever is first.
        # We can start a new generation.
        elif event == events.DEATH:
            start = time.time() # Not sure if this is what we want?

            # Purge any remaining entities from population.people
            universe.remove_entities(population.people)

            # Get the children from the population
            # I do this after removing them from the universe because
            # .evolve() modifies .people
            children = population.evolve(selection, ga.mutate, fitness, 0.05)

            # Add the new children.
            universe.add_entities(children)

            # Handle some generation logic.
            # This increases the number of moves every few generations.
            # Doing this makes each generations more likely
            # to master the limit they have and pass on better genes.
            generation += 1
            if generation == 5:
                moves += 5
                life = ga.apply_life(universe, ga.life(population, moves))
                generation = 0

        # UI stuffs
        ui.handle_events()
        ui.draw_population(window, population.people)
        ui.draw_goal(window, goal)
        ui.draw_obstacles(window, obstacles)
        ui.update()
    return None


if __name__ == "__main__":
    main()
