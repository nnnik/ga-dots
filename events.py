import enum
import itertools


class Event(enum.Enum):
    TICK = 0
    LIFE = 1
    DEATH = 2


TICK = Event.TICK
DEATH = Event.DEATH
LIFE = Event.LIFE

UNIVERSE = itertools.repeat(TICK)
